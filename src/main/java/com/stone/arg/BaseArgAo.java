package com.stone.arg;

import java.io.Serializable;

public interface BaseArgAo extends Serializable{
	public void checkArgs();
}
