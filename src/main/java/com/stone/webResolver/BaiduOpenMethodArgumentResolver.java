package com.stone.webResolver;


import com.stone.arg.BaseArgAo;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;

public class BaiduOpenMethodArgumentResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter (MethodParameter methodParameter) {
        Class<?> paramType = methodParameter.getParameterType();
        return BaseArgAo.class.isAssignableFrom(paramType);
    }

    @Override
    public Object resolveArgument (MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
        HttpServletRequest request = nativeWebRequest.getNativeRequest(HttpServletRequest.class);

        String argRequest = (String) request.getAttribute("packet");
        Class<?> paramType = methodParameter.getParameterType();

        //解析并且校验参数
        BaseArgAo arg = (BaseArgAo) JsonUtil.fromJson(argRequest, paramType);
        arg.checkArgs();
        return arg;
    }
}
