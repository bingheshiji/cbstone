package com.stone.filter;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class MyFilter implements Filter {

    private static final String CURRENT_USER = "current_user";

    protected static List<Pattern> patterns = new ArrayList<Pattern>();

    static {
      patterns.add(Pattern.compile("/fitter"));
      patterns.add(Pattern.compile("/login"));
      patterns.add(Pattern.compile("/register"));
    }



    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper(response);
        String url = request.getRequestURI().substring(request.getContextPath().length());
        HttpSession session = request.getSession();
        if(isInclude(url)){
            filterChain.doFilter(request,response);
            return;
        }

        if (session.getAttribute(CURRENT_USER) != null) {
            //若为登录状态 放行访问
            filterChain.doFilter(request,response);
            return;
        } else {
            //否则默认访问index接口
            wrapper.sendRedirect("/fitter");
        }


    }

    @Override
    public void destroy() {

    }

    private boolean isInclude(String url){
        for (Pattern pattern : patterns){
            Matcher matcher = pattern.matcher(url);
            if (matcher.matches()){
                return true;
            }
        }
        return false;
    }
}
