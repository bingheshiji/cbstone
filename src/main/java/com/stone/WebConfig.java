package com.stone;

import com.stone.filter.MyFilter;
import com.stone.webResolver.BaiduOpenMethodArgumentResolver;
import com.stone.webResolver.BaiduOpenMethodReturnValueHandler;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import javax.servlet.FilterRegistration;
import java.util.List;

@Configurable
public class WebConfig extends WebMvcConfigurationSupport {

    public FilterRegistrationBean someFitterRegistration(){
        FilterRegistrationBean filter = new FilterRegistrationBean();
        filter.setFilter(new MyFilter());
        filter.setName("myFilter");
        return filter;
    }


    @Bean
    public BaiduOpenMethodArgumentResolver baiduOpenMethodArgumentResolver(){
        return new BaiduOpenMethodArgumentResolver();
    }

    /**
     * 参数解析器的配置，解析传入后台的参数
     */
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        super.addArgumentResolvers(argumentResolvers);
        argumentResolvers.add(baiduOpenMethodArgumentResolver());
    }

    @Bean
    public BaiduOpenMethodReturnValueHandler openPlatformMethodReturnValueHandler(){
        return new BaiduOpenMethodReturnValueHandler();
    }
    /**
     * 返回数据自定义封装
     */
    @Override
    public void addReturnValueHandlers(List<HandlerMethodReturnValueHandler> returnValueHandlers) {
        super.addReturnValueHandlers(returnValueHandlers);
        returnValueHandlers.add(openPlatformMethodReturnValueHandler());
    }
}
